-- [SECTION] Inserting Records/Row

-- Syntax: INSERT INTO <table_name> (<column_name>, <column_name>, <column_name>) VALUES (<value>, <value>, <value>);

-- insert into artists table
INSERT INTO artists(artist_name) VALUES ('Rivermaya');
INSERT INTO artists(artist_name) VALUES ('Parokya ni Edgar');
INSERT INTO artists(artist_name) VALUES ('Ely Buendia');
INSERT INTO artists(artist_name) VALUES ('Taylor Swift');
INSERT INTO artists(artist_name) VALUES ('New Jeans');
INSERT INTO artists(artist_name) VALUES ('Bamboo');
INSERT INTO artists(artist_name) VALUES ('Black Pink');

-- insert into albums table
INSERT INTO albums(album_title, date_released, artist_id) VALUES ('Born Pink', '2022-09-16', 1001);
INSERT INTO albums(album_title, date_released, artist_id) VALUES ('The Album', '2020-10-22', 1001);
INSERT INTO albums(album_title, date_released, artist_id) VALUES ('Midnights', '2022-10-21', 4);
INSERT INTO albums(album_title, date_released, artist_id) VALUES ('New Jeans', '2022-10-21', 1001);
INSERT INTO albums(album_title, date_released, artist_id) VALUES ("Trip", "1996-01-01", 999);
INSERT INTO albums(album_title, date_released, artist_id) VALUES ('Light is Love', '2022-10-21', 6);
INSERT INTO albums(album_title, date_released, artist_id) VALUES ('Light is Love', '2022-10-21', 6);

-- insert into songs table
INSERT INTO songs(song_name, length, genre, album_id)
    VALUES ('Snow on the beach', 256, 'Pop', 7),
    ('Anti-Hero', 201, 'Pop', 7) ;

INSERT INTO songs(song_name, length, genre, album_id)
    VALUES ('Bejeweled', 314, 'Pop', 7);

INSERT INTO songs(song_name, genre, album_id)
    VALUES ('Paris', 'Pop', 7);


INSERT INTO songs(song_name, length, genre, album_id)
    VALUES ('Masaya', 450, 'Jazz', 13),
     ('Mr. Clay', 357, 'Jazz', 13),
    ('Noypi', 400, 'Jazz', 13);

INSERT INTO songs(song_name, length, genre, album_id)
    VALUES ('The Yes Yes Show', "00:04:53", 'Comedy-gag', 11),
    ('Mr. Suave', "00:0:26", 'Pop', 11) ;

INSERT INTO songs(song_name, length, genre, album_id)
    VALUES ('The Ordertaker', "00:05:09", 'Rock', 10),
    ('Bagsakan', "00:04:19", 'Rock', 10) ;

INSERT INTO songs(song_name, length, genre, album_id)
    VALUES ('War of Hearts and Minds', "00:03:54", 'Pop', 12),
    ('The General', "00:04:10", 'Pop', 12) ;

INSERT INTO songs(song_name, length, genre, album_id)
    VALUES ('Kisapmata', "00:04:41", 'Pop', 9),
    ('Flowers', "00:05:39", 'Pop', 9) ;


-- [SECTION] READ/SELECT data from our database
-- Syntax: SELECT <column_name>, <column_name>, <column_name> FROM <table_name>;

SELECT * FROM songs;

-- Specify the columns to be selected
SELECT song_name FROM songs;
SELECT song_name genre FROM songs;

-- filter the data of the output using WHERE clause
SELECT * FROM songs WHERE genre = 'Pop';
SELECT song_name FROM songs WHERE genre = 'Pop';
SELECT * FROM albums WHERE artist_id = 4;


--  we can use AND and OR keyword for multiple expressions in the WHERE clause
-- Display the title and length of the songs that are longer than 4 minutes
SELECT song_name, length FROM songs WHERE length > 400 and genre = 'Pop';

SELECT song_name, length, genre FROM songs WHERE length >= 400 AND genre = 'Pop' AND genre = 'Jazz';


-- [SECTION] UPDATE data in our database
-- Syntax: UPDATE <table_name> SET <column_name> = <value>, <column_name> = <value> WHERE <condition>;
UPDATE songs SET length = 300 WHERE song_name = 'Anti-Hero';
-- IS NULL is used to check if the value is NULL

-- [SECTION] DELETE data in our database
-- Syntax: DELETE FROM <table_name> WHERE <condition>;

-- Delete all POP songs that is more than 4 minutes
DELETE FROM songs WHERE genre = 'Pop' AND length > 400;

-- Delete all Rock songs that is more than 5 minutes
DELETE FROM songs WHERE genre = 'Rock' AND length > 500;

